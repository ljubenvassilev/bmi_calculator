import 'package:flutter/material.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'BMI',
      home: MyHomePage(title: 'BMI'),
      debugShowCheckedModeBanner: false
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  final TextEditingController _ageTextController = TextEditingController();
  final TextEditingController _heightTextController = TextEditingController();
  final TextEditingController _weightTextController = TextEditingController();
  String _resultDisplay = "";
  String _conditionDisplay = "";

  void _handleCalculateClicked(){
    if (_ageTextController.text.isEmpty ||
        _heightTextController.text.isEmpty ||
        _weightTextController.text.isEmpty ||
        int.parse(_heightTextController.text).toString().isEmpty ||
        int.parse(_heightTextController.text) <= 0 ||
        int.parse(_weightTextController.text).toString().isEmpty ||
        int.parse(_weightTextController.text) <= 0) {
      return;
    }

    int height = int.parse(_heightTextController.text);
    int weight = int.parse(_weightTextController.text);
    double bmi = calculateBMI(height, weight);

    setState(() {
      _resultDisplay = "Your BMI: ${bmi.toStringAsFixed(1)}";
      _conditionDisplay = "${getHealthStatus(bmi)}";
    });
  }

  double calculateBMI(int height, int weight){
    return (weight / (height * height)) * 10000;
  }

  String getHealthStatus(double bmi){
    if (bmi < 18.5) {
      return "Underweight";
    } else {
      if (bmi < 24.9) {
        return "Healthy";
      } else {
        if (bmi < 29.9) {
          return "Overweight";
        } else {
          return "Obese";
        }
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pinkAccent,
        centerTitle: true,
        title: Text(
          widget.title,
          style: TextStyle(
            fontSize: 25.0
          ),
        ),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Image.asset(
              'images/bmilogo.png',
              height: 120.0,
              width: 150.0,
            ),
            Container(
              color: Colors.grey.shade200,
              padding: EdgeInsets.all(8.0),
              child: Column(
                children: <Widget>[
                  TextField(
                    controller: _ageTextController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Age",
                        icon: Icon(
                            Icons.person_outline
                        )
                    ),
                  ),
                  TextField(
                    controller: _heightTextController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Height in centimeters",
                        icon: Icon(
                            Icons.insert_chart
                        )
                    ),
                  ),
                  TextField(
                    controller: _weightTextController,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                        labelText: "Weight in kilograms",
                        icon: Icon(
                            Icons.dehaze
                        )
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.only(
                      left: 0.0,
                      right: 0.0,
                      bottom: 5.0,
                      top: 18.0
                    ),
                    child: RaisedButton(
                      onPressed: _handleCalculateClicked,
                      color: Colors.pinkAccent,
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          "Calculate",
                          style: TextStyle(
                            fontSize: 18.0,
                            color: Colors.white
                          ),
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(12.0),
              child: Text(
                _resultDisplay,
                style: TextStyle(
                  fontWeight: FontWeight.bold,
                  fontStyle: FontStyle.italic,
                  color: Colors.blue,
                  fontSize: 27.0
                ),
              ),
            ),
            Text(
              _conditionDisplay,
              style: TextStyle(
                  fontWeight: FontWeight.bold,
                  color: Colors.pinkAccent,
                  fontSize: 27.0
              ),
            )
          ],
        ),
      ),
    );
  }
}
